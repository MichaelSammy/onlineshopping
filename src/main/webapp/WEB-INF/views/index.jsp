<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>$Title$</title>
    <style>
        div {
            text-align: center;
            margin-top: 168px;
            font-size: larger;
        }

        div > p {
            color: olivedrab;
        }

        .fontClass {
            color: deeppink;

        }

    </style>
</head>
<body>
<div><p>projectName</p><p class="fontClass">${projectName}</p></div>
</body>
</html>