package com.isstech.onlineShopping.controller;

import com.isstech.onlineShopping.service.UserService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletResponse;


@Controller
public class TestController {
    @Autowired
    private UserService userService;

    @RequestMapping("/index")
    public ModelAndView test(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView();
        int resultNum=userService.insertUser("张三","123456");
        if(resultNum>0){
            modelAndView.addObject("projectName", "onlineShopping");
        }else{
            modelAndView.addObject("projectName", "添加失败！");
        }
        modelAndView.setViewName("index");
        return modelAndView;
    }
}
