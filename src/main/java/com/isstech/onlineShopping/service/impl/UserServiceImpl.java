package com.isstech.onlineShopping.service.impl;

import com.isstech.onlineShopping.dao.UserDao;
import com.isstech.onlineShopping.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;


@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    @Override
    public int insertUser(String userName, String password) {
        Map map = new HashMap();
        map.put("userName", userName);
        map.put("password", password);
        return userDao.insertUser(map);
    }
}
